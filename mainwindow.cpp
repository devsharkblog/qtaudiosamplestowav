#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    samplesBuffer = new QVector <qint32> ();

    //  create recorder  //
    audioRecorder = new QAudioRecorder(this);

    //  configure recorder  //
    QAudioEncoderSettings audioSettings;
    audioSettings.setCodec("audio/PCM");
    audioSettings.setQuality(QMultimedia::HighQuality);
    audioSettings.setChannelCount(1);
    audioRecorder->setEncodingSettings(audioSettings);

    //  set destination  //
    audioRecorder->setOutputLocation(QUrl::fromLocalFile("recorded-probe-samp.wav"));

    //  create probe  //
    audioProbe = new QAudioProbe(this);
    connect(audioProbe,
            SIGNAL(audioBufferProbed(QAudioBuffer)),
            this,
            SLOT(processBuffer(QAudioBuffer)));

    //  set source  //
    if(!audioProbe->setSource(audioRecorder))
    {
        qDebug() << "Source fail!";
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}

bool MainWindow::saveWAV()
{
    //  configure format  //
    SF_INFO sndFileInfo;
    sndFileInfo.channels = 1;
    sndFileInfo.samplerate = 44100;
    sndFileInfo.format = SF_FORMAT_WAV | SF_FORMAT_PCM_32;

    QString filePath = "customWAV-" + QString::number(QDateTime::currentMSecsSinceEpoch()) + ".wav";

    //  open file  //
    SNDFILE *sndFile = sf_open(filePath.toStdString().c_str(),
                               SFM_WRITE,
                               &sndFileInfo);

    //  check file was opened  //
    if(sndFile != NULL)
    {
        //  write samples  //
        sf_count_t count = sf_write_int(sndFile,
                                        samplesBuffer->data(),
                                        samplesBuffer->count());

        //  debug time  //
        qDebug() << "Written "
                 << count << " items; "
                 << (samplesBuffer->count() / 44100.0) << " seconds";
    }
    else
    {
        //  get error string  //
        QString errStr = sf_strerror(sndFile);

        //  debug error  //
        qDebug() << "Save WAV err: " << errStr;

        return false;
    }

    //  close file  //
    sf_close(sndFile);

    return true;
}


void MainWindow::processBuffer(QAudioBuffer audioBuffer)
{
    //  get 32bit data  //
    const qint32 *data32b = audioBuffer.constData<qint32>();

    //  add data to buffer  //
    for(int i = 0; i < audioBuffer.sampleCount(); i++)
    {
        samplesBuffer->append(data32b[i]);
    }
}

void MainWindow::on_pushButton_start_clicked()
{
    //  start  //
    audioRecorder->record();

    samplesBuffer->clear();

    qDebug() << "Recording started";
}

void MainWindow::on_pushButton_stop_clicked()
{
    audioRecorder->stop();

    qDebug() << "Recording finished";

    saveWAV();
}
