#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QDebug>
#include <QAudioRecorder>
#include <QUrl>
#include <QAudioProbe>
#include <QAudioInput>
#include <QDateTime>

#include <sndfile.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

    QAudioRecorder *audioRecorder;

    QAudioProbe *audioProbe;

    QAudioInput *audioInput;

    QVector <qint32> *samplesBuffer;

private:
    bool saveWAV();

private slots:
    void processBuffer(QAudioBuffer audioBuffer);
    void on_pushButton_start_clicked();
    void on_pushButton_stop_clicked();
};

#endif // MAINWINDOW_H
